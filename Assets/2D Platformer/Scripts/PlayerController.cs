using System.Collections;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movingSpeed;
    public float jumpForce;
    private float moveInput;

    private bool facingRight = false;
    [HideInInspector] public bool deathState = false;

    private bool isGrounded;
    public Transform groundCheck;
    public Transform groundCheck2;

    private Rigidbody2D rigidbody;
    private Animator animator;
    private CapsuleCollider2D _boxCollider2D;

    [SerializeField] private Transform grafiksLis;
    [SerializeField] private Transform gunPoint;
    [SerializeField] private Transform flashPoint;
    [SerializeField] private Bullet bulletPrefab;
    [SerializeField] private Transform parentBullet;

    private CameraController _cameraController;
    private bool IsDead;
    
    private bool right;
    private float futureTime;
    private bool isMoveMobile;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = grafiksLis.GetComponent<Animator>();
        _cameraController = FindObjectOfType<CameraController>();
        _boxCollider2D = GetComponent<CapsuleCollider2D>();
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    public void PressRightOn()
    {
        isMoveMobile = true;
        moveInput = 1;
    }
    
    public void PressRightOf()
    {
        isMoveMobile = false;
    }
    
    public void PressLeftOn()
    {
        isMoveMobile = true;
        moveInput = -1;
    }
     
    public void PressLeftOf()
    {
        isMoveMobile = false;
    }

    private void Move()
    {
        if (Input.GetButton("Horizontal") || isMoveMobile)
        {
            if (!isMoveMobile)
            {
                moveInput = Input.GetAxis("Horizontal");
            }
             
            movingSpeed = isGrounded ? 6 : 4;
            
            Vector3 direction = transform.right * moveInput;

            if (direction.x > 0)
            {
                right = true;
            }
            else
            {
                right = false;
            }

            transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, movingSpeed * Time.deltaTime);
            animator.SetInteger("playerState", 1); // Turn on run animation
        }
        else
        {
            if (isGrounded) animator.SetInteger("playerState", 0); // Turn on idle animation
        }

        if (!isGrounded)
        {
            animator.SetInteger("playerState", 2);
        }
 
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        } 
    }

    void Update()
    {
        if (GameManager.Instance.isPause)
        {
            return;
        }
        
        if (IsDead)
        {
            return;
        }
        
        Move();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.Return))
        { 
            Fire();
        }
    }

    private void Fire()
    {
        if (GameManager.Instance.isPause)
        {
            return;
        }
        
        if (futureTime >= Time.time)
        {
            return;
        }
        
        Bullet bullet = LeanPool.Spawn(bulletPrefab, gunPoint);
        AudioManager.Instance.PlayShot();
        EffectorManager.Instance.SpawnShootFlash(flashPoint);

        if (right)
        {
            bullet.ShootRight();
        }
        else
        {
            bullet.ShootLeft();
        }

        bullet.transform.parent = parentBullet;

        futureTime = Time.time + 0.5f;
    }

    private void Jump()
    {
        if (GameManager.Instance.isPause)
        {
            return;
        }
        
        if (!isGrounded)
        {
            return;
        }
        
        AudioManager.Instance.PlayJump();
        rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }
    
    public void MobileJump()
    {
        Jump();
    }      
         
    public void MobileFire()
    {
        Fire();
    } 
    
    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = grafiksLis.transform.localScale;
        Scaler.x *= -1;
        grafiksLis.transform.localScale = Scaler;
    }

    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.transform.position, 0.2f);
        Collider2D[] colliders2 = Physics2D.OverlapCircleAll(groundCheck2.transform.position, 0.2f);

        isGrounded = colliders.Length > 1 || colliders2.Length > 1;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other == null)
        {
            return;
        }
        
        if (other.CompareTag("fly"))
        {
            Enemy enemy = other.GetComponent<Enemy>();

            if (enemy.IsStun)
            {
                print("у врага стан");
            }
            else
            {
                print(" dead");
                Dead();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Aid"))
        {
            Dead();
        }
        
        if (other.CompareTag("Star"))
        {
            AudioManager.Instance.PlayStarCollect();
            print("star");
            Star star = other.GetComponent<Star>();
            star.Collect();
        }
    }

    private void Dead()
    {
        AudioManager.Instance.GameOver();
        _cameraController.IsStopCamera = true;
        rigidbody.gravityScale = 0.65f;
        rigidbody.mass = 0.1f;
        _boxCollider2D.enabled = false;
        IsDead = true;
        transform.DOLocalRotate(new Vector3(0, 0, 720), 5, RotateMode.FastBeyond360);
        StartCoroutine(StageGail());
    }

    IEnumerator StageGail()
    {
        yield return new WaitForSecondsRealtime(1);
        GameManager.Instance.stageFail.Show();
    }
}