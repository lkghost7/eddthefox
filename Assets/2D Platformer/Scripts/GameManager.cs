using TMPro;
using UnityEngine;

public class GameManager : GenericSingletonClass<GameManager>
{
    [SerializeField] private Transform starsParent;
    public StageFailPopup stageFail;
    public WinPanel winPanel;
    public PausePanel PausePanel;
    [SerializeField] private TextMeshProUGUI scoreStars;

    public bool isPause { get; set; }
    
    private int starsLevels;
    private int currentStars;
    
    private int currentCheckStars;
    
    private void Start()
    {
        int count = starsParent.transform.childCount; 
        for (int i = 0; i < count; i++)
        {
            starsLevels++;
        }

        currentCheckStars = starsLevels;
        scoreStars.text = currentCheckStars.ToString();
    }
 
    public void AddCollect()
    {
        currentStars++;
        currentCheckStars--;
        scoreStars.text = currentCheckStars.ToString();
        ChekWin();
    }
   
    private void ChekWin()
    {
        if (currentStars >= starsLevels)
        {
            winPanel.Show();
        }
    }
}