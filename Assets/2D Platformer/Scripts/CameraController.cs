using System;
using UnityEngine;

    public class CameraController : MonoBehaviour
    {
        public float damping = 1.5f;
        public bool faceLeft;
        private Transform player;
        private int lastX;
        private float leftClamp;
        private float rightClamp;
        private float screenAspect;
        private Vector2 lastScreenSize;

        [SerializeField] private float clamp;
        

        public bool IsStopCamera { get; set; }

        void Start()
        {
            FindPlayer(faceLeft);
            UpdateClamps();
            lastScreenSize = new Vector2(Screen.width, Screen.height);
        }

        void Update()
        {
            if (lastScreenSize.x != Screen.width || lastScreenSize.y != Screen.height)
            {
                UpdateClamps();
                lastScreenSize = new Vector2(Screen.width, Screen.height);
            }
        }

        private void UpdateClamps()
        {
            screenAspect = (float)Screen.width / Screen.height;
            double screenResult = Math.Round(screenAspect, 1);

            if (screenResult == 1.2 || screenResult == 1.3 || screenResult == 1.4 || screenResult == 1.5)
            {
                leftClamp = -8.5f;
                rightClamp = 5.5f;
            }
            else if (screenResult == 1.9 || screenResult == 2 || screenResult == 2.1)
            {
                leftClamp = -5f;
                rightClamp = 2f;
            }
            else if (screenResult == 1.6 || screenResult == 1.7 || screenResult == 1.8)
            {
                leftClamp = -5.6f;
                rightClamp = 3f;
            }
            else if (screenResult == 2.2 || screenResult == 2.3 || screenResult == 2.4)
            {
                leftClamp = -3f;
                rightClamp = 0f;
            }
        }

        public void FindPlayer(bool playerFaceLeft)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
            lastX = Mathf.RoundToInt(player.position.x);
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
        }

        void FixedUpdate()
        {
            if (IsStopCamera || !player) return;

            int currentX = Mathf.RoundToInt(player.position.x);
            faceLeft = currentX < lastX;
            lastX = currentX;

            Vector3 target = new Vector3(player.position.x, player.position.y, transform.position.z);
            Vector3 currentPosition = Vector3.Lerp(transform.position, target, damping * Time.deltaTime);
            float clampX = Mathf.Clamp(currentPosition.x, leftClamp, rightClamp);
            float clampY = Mathf.Clamp(currentPosition.y, 2, 93);
            transform.position = new Vector3(clampX, clampY, currentPosition.z);
        }
    
}
