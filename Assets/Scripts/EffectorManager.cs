using System.Collections;
using Lean.Pool;
using UnityEngine;

public class EffectorManager : GenericSingletonClass<EffectorManager>
{
    [SerializeField] private GameObject bulletWall;
    [SerializeField] private GameObject shootFlash;
    
    public void SpawnBulletWall(Transform wall)
    {
        GameObject bullet = LeanPool.Spawn(bulletWall, wall);
        bullet.transform.SetParent(this.transform);
        StartCoroutine(Despawner(bullet));
    }
    
    public void SpawnShootFlash(Transform flash)
    {
        GameObject shoot = LeanPool.Spawn(shootFlash, flash);
        shoot.transform.SetParent(this.transform);
        StartCoroutine(Despawner(shoot));
    }

     IEnumerator Despawner(GameObject despObj)
     {
         yield return new WaitForSeconds(2);
         LeanPool.Despawn(despObj);
     }
}
