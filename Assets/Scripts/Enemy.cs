using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Core.PathCore;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator animator;
    private BoxCollider2D bc;
    private Rigidbody2D rigidbody2D;
    private SpriteRenderer spriteRenderer;

    [SerializeField] private ParticleSystem stunEffect;
    [SerializeField] private ParticleSystem stunStar;
    [SerializeField] private float timeDuration = 3;
     private readonly float timeFreeze = 7;

    private Coroutine currentStun;

    public NodeCollect nodeCollect;
    [Header("point")]
    [SerializeField] private Transform point1;
    [SerializeField] private Transform point2;
    
    private TweenerCore<Vector3, Path, PathOptions> moveTween;
    private TweenerCore<Vector3, Vector3, VectorOptions> moveTween2;
    private TweenerCore<Vector3, Vector3, VectorOptions> moveTween3;
    public bool IsStun { get; private set; }

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        bc = GetComponent<BoxCollider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        
        MoveToNode();
    }

    private void MoveToNode()
    {
        if (nodeCollect == null)
        {
            MoveToPoint();
            return;
        }
       
        Vector3[] nodVector = nodeCollect.vectorListNode.ToArray();
        moveTween = transform.DOPath(nodVector, timeDuration, PathType.Linear, PathMode.Sidescroller2D).SetEase(Ease.Linear).OnComplete(() => { MoveToNode(); });
    }

    private void MoveToPoint()
    {
        spriteRenderer.flipX = false;
        moveTween2 = transform.DOMove(point1.position, 3f)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                spriteRenderer.flipX = true;
                moveTween3 = transform.DOMove(point2.position, 3f).SetEase(Ease.Linear).OnComplete(MoveToPoint);
            });
    } 
  
    public void Stun()
    {
        moveTween2.Pause();
        moveTween3.Pause();
        moveTween.Pause();
        stunEffect.Play();
        
        if (currentStun != null)
        {
            StopCoroutine(currentStun);
        }
        
        currentStun = StartCoroutine(StunStarEffect());
    }
    
    IEnumerator StunStarEffect()
    {
        animator.speed = 0.0f;
        IsStun = true;
        stunStar.Play();
        yield return new WaitForSeconds(timeFreeze);
        animator.speed = 1f;
        stunStar.Stop();
        IsStun = false;
        moveTween.Play();
        moveTween2.Play();
        moveTween3.Play();
    }
}