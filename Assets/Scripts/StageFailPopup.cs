using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageFailPopup : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    
    public void Show()
    {
        gameObject.SetActive(true);
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 1);
    }

    public void Hide()
    {
        canvasGroup.DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
 
    public void RestartBtn()
    {
        SceneManager.LoadScene(1);
    }

    public void HomeExit()
    {
        SceneManager.LoadScene(0);
    }
}
