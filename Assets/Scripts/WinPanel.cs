using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinPanel : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    
    public void Show()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0;
        GameManager.Instance.isPause = true;
        AudioManager.Instance.PauseOn();
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 0.4f).SetUpdate(true);
  
    }
      
    public void Hide()
    { 
        Time.timeScale = 1;
        GameManager.Instance.isPause = false;
        AudioManager.Instance.PauseOf();
        canvasGroup.DOFade(0, 0.4f).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
      
    public void RestartBtn()
    {
        Time.timeScale = 1;
        GameManager.Instance.isPause = false;
        AudioManager.Instance.PauseOf();
        SceneManager.LoadScene(1);
    }
 
    public void HomeExit()
    {
        Time.timeScale = 1;
        GameManager.Instance.isPause = false;
        AudioManager.Instance.PauseOf();
        SceneManager.LoadScene(0);
    }

}
