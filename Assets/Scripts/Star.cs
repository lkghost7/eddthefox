using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] private ParticleSystem effectStar;
    private SpriteRenderer _spriteRenderer;
    private TweenerCore<Quaternion, Vector3, QuaternionOptions> currentTween;
    private BoxCollider2D _boxCollider2D;

    private void Start()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        currentTween = transform.DORotate(new Vector3(0, 360, 0), 3, RotateMode.FastBeyond360).SetLoops(100000);
    }

    public void Collect()
    {
        _boxCollider2D.enabled = false;
        currentTween.Kill();
        _spriteRenderer.enabled = false;
        effectStar.Play();
        GameManager.Instance.AddCollect();
        Destroy(gameObject, 0.2f);
    }
}