using System.Collections.Generic;
using UnityEngine;

public class NodeCollect : MonoBehaviour
{
    public List<Vector3> vectorListNode { get; private set; } = new List<Vector3>();

    private void Awake()
    {
        int count = transform.childCount; 
        for (int i = 0; i < count; i++)
        {
            Transform item = transform.GetChild(i).transform;
            vectorListNode.Add(item.position);
        }
    }
}