using System.Collections;
using UnityEngine;

public class AudioManager : GenericSingletonClass<AudioManager>
{
    [SerializeField] private AudioClip shotSound;
    [SerializeField] private AudioClip starCollect;
    [SerializeField] private AudioClip jump;
    [SerializeField] private AudioClip takeDamage;
    [SerializeField] private AudioClip takeWall;
    [SerializeField] private AudioClip reload;
    [SerializeField] private AudioClip over;
    [SerializeField] private AudioSource audioSource;


    public void PlayTrack()
    {
        audioSource.Stop();
    }
    public void PlayShot()
    {
        audioSource.PlayOneShot(shotSound);
    }

    public void PlayStarCollect()
    {
        audioSource.PlayOneShot(starCollect);
    }

    public void PlayJump()
    {
        audioSource.PlayOneShot(jump);
    }

    public void PlayTakeDamageEnemy()
    {
        audioSource.PlayOneShot(takeDamage);
    }
    
    public void PlayTakeWall()
    {
        audioSource.PlayOneShot(takeWall);
    }
    
    private IEnumerator PlayReload()
    {
        yield return new WaitForSeconds(0.2f);
        audioSource.PlayOneShot(reload);
    }
    
    public void GameOver()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(over);
    }

    public void PauseOn()
    {
        audioSource.Pause();
    }
    
    public void PauseOf()
    {
        audioSource.Play();
    }

}
