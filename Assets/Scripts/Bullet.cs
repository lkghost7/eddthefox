using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D rd;

    private void Awake() {
        rd = GetComponent<Rigidbody2D>();
    }

    public void ShootRight()
    {
        rd.linearVelocity = Vector2.right * 15;  
    }
    
    public void ShootLeft()
    {
        rd.linearVelocity = Vector2.left * 15;   
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
        {
            return;
        } 
         
        if (other.CompareTag("fly"))
        {
            print("flu");
            other.GetComponent<Enemy>().Stun();
            AudioManager.Instance.PlayTakeDamageEnemy();
            Destroy(gameObject, 0.05f);
        }
        
        if (other.CompareTag("Wall"))
        {
            // AudioManager.Instance.PlayTakeWall();
            EffectorManager.Instance.SpawnBulletWall(this.transform);
            Destroy(gameObject, 0.03f);
        }   
    }
}
