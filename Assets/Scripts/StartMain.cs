using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMain : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
 
    public void ExitGame()
    {
        Application.Quit();
    }
}
